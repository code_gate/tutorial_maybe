using System;
using System.Collections.Generic;
using System.Linq;
using Functional.Maybe;

namespace TutorialMaybe
{
    public class MaybeType
    {
        public static void MaybeExample()
        {
            int[] numbers = new[]{1, 2, 3, 4, 5};
            Maybe<int> maybeA = numbers.FirstMaybe();
//            Maybe<int> maybeB = Maybe<int>.Nothing;
            Maybe<int> maybeB = numbers.LastMaybe();

            //result will be 6 if a and b has value, otherwise it will be -1
            int result = (
                from a in maybeA
                from b in maybeB
                select a + b).OrElse(-1);
            //OR
//            result = (from a in numbers.FirstMaybe() from b in numbers.LastMaybe() select a + b).OrElse(-1);
            
            Console.WriteLine(result);
        }

        public static void ClassicExample()
        {
            var numbers = new[] {1, 2, 3, 4, 5};
            int? aNumber = numbers.First();
            int? bNumber = numbers.Last();
            int result = 0;
            
            if (aNumber.HasValue && bNumber.HasValue)
            {
                result = aNumber.Value + bNumber.Value;
            }
            else
            {
                result = -1;
            }
            
            Console.WriteLine(result);
        }
    }
}