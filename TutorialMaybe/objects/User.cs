namespace TutorialMaybe.objects
{
    public class User : BaseUser
    {
        public Work Work { get; set; }
        
        public User(string name2, string name1)
        {
            Name2 = name2;
            Name1 = name1;
        }

        public string Name1 { get; set; }
        public string Name2 { get; set; }
    }
}