using System;
using System.Collections.Generic;
using Functional.Maybe;

namespace TutorialMaybe
{
    public class MaybeDictionaryExamples
    {
        public static void MaybeExample()
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("1", null);
            dic.Add("2", "value2");
            dic.Add("3", null);

            Maybe<string> value = dic.Lookup("value1");
            Console.WriteLine(value.HasValue);
        }
    }
}