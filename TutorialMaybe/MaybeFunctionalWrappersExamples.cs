using System;
using Functional.Maybe;

namespace TutorialMaybe
{
    public class MaybeFunctionalWrappersExamples
    {
        public static void MaybeExample()
        {
            Maybe<int> number = TryParseInt("1");
        }
        
        public static Maybe<int> TryParseInt(string s) {
            Func<string, Maybe<int>> parser = MaybeFunctionalWrappers.Wrap<string, int>(int.TryParse);
            return parser(s);
        }
    }
}