using System;
using Functional.Maybe;
using TutorialMaybe.objects;

namespace TutorialMaybe
{
    public class MaybeConversionsExamples
    {
        public static void MaybeExample()
        {
//            BaseUser user = new User("Jan", "Kowalski");
            object user = new User("Jan", "Kowalski");
            
            //It will try cast object, if operation fails, return Maybe object without value
            var castedUser = user.MaybeCast<object, User>();

            Console.WriteLine(castedUser.HasValue);
        }

        public static void ClassicExample()
        {
            object user = new User("Jan", "Kowalski");
            User castedUser;
            if (user != null)
            {
                try
                {
                    castedUser = (User) user;
                }
                catch
                {
                }
            }
        }
    }
}