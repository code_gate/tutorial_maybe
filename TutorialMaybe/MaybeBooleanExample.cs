using System;
using System.Diagnostics;
using Functional.Maybe;

namespace TutorialMaybe
{
    public class MaybeBooleanExample
    {
        public static void MaybeExample()
        {
            bool SomeBoolean = true;
            
            //Executes action from first parameter if variable is true, otherwise executes second action
            SomeBoolean.ToMaybe().DoWhenTrue(someMethodWhenTrue, someMethodWhenFalse);
        }

        public static void ClassicExample()
        {
            bool somethingTrue = true;

            if (somethingTrue)
            {
                someMethodWhenTrue();
            }
            else
            {
                someMethodWhenFalse();
            }
        }

        public static void MaybeExample2()
        {
            var SomeBoolean = false;
            
            //if variable is true, return value from 'Then', otherwise returns value from 'Or'
            Maybe<string> number = SomeBoolean.Then("Is true").Or("Is false");
            
//            Console.WriteLine(number.Value);
            number.Do(Console.WriteLine);
        }

        private static void someMethodWhenTrue()
        {
            Console.WriteLine("Condition was true");
        }

        private static void someMethodWhenFalse()
        {
            Console.WriteLine("Condition was false");
        }
    }
}