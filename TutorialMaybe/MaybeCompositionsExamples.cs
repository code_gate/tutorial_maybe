using System;
using Functional.Maybe;
using TutorialMaybe.objects;

namespace TutorialMaybe
{
    public class MaybeCompositionsExamples
    {
        public static void MaybeExample()
        {
            Maybe<string> name1 = Maybe<string>.Nothing;
            Maybe<string> name2 = "Some Name".ToMaybe();
            Maybe<string> goodName;
                
            //returns name1 is not nothing, otherwise name2
            goodName = name1.Or(() => name2);
            goodName = name1.Or(name2);
            goodName = name1.Or("goodName");
            
            Console.WriteLine(goodName);
            //or
            name1.Match(Console.WriteLine, () => Console.WriteLine(name2));
        }

        public static void ClassicExample()
        {
            string name1 = null;
            string name2 = "Some name";
            string goodName = "";

            if (name1 != null)
            {
                goodName = name1;
            }
            else
            {
                goodName = name2;
            }
            
            Console.WriteLine(goodName);
        }

        public static void MaybeExample2()
        {
            User User = null;
            
            // If User is not null, return 5 in Maybe type, otherwise returns Maybe without value
            Maybe<int> a = User.ToMaybe().Compose(5.ToMaybe());
            
            //returns false because User is nothing
            Console.WriteLine(a.HasValue);
        }
    }
}