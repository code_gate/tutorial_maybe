using System;
using Functional.Maybe;
using TutorialMaybe.objects;

namespace TutorialMaybe
{
    public class MaybeNullableExample
    {
        public static void MaybeExtracting()
        {
            Maybe<string> name1 = Maybe<string>.Nothing;
            Maybe<string> name2 = Maybe<string>.Nothing;
            string result = "";

            result = name1.Or(name2).Or("EMPTY!!!").Value;
            
            Console.WriteLine(result);
        }

        public static void Extracting()
        {
            string name1 = null;
            string name2 = null;
            string result = "";

            if (name1 != null)
            {
                result = name1;
            }
            else if (name2 != null)
            {
                result = name2;
            }
            else
            {
                result = "EMPTY!!!";
            }
            
            Console.WriteLine(result);
        }

        public static void MaybeExample()
        {
            User user = new User("Jan", "Kowalski");
            
            user?.Work?.Spec.Experience.ToMaybe().Do(Console.WriteLine);
        }

        public static void ClassicExample()
        {
            User user = new User("Jan", "Kowalski");
            if (user != null && user.Work != null && user.Work.Spec != null && user.Work.Spec.Experience.HasValue)
            {
                Console.WriteLine(user.Work.Spec.Experience.Value);
            }
            //OR
            var Exp = user?.Work?.Spec?.Experience;
            if (Exp.HasValue)
            {
                Console.WriteLine(Exp.Value);
            }
        }

        public static void MaybeExample2()
        {
            var text = Maybe<string>.Nothing;
            text.Match(ActionOnNotNull, ActionOnNull);
            //or
            text.Match(Console.WriteLine, () => Console.WriteLine("NULL"));
        }

        private static void ActionOnNull()
        {
            Console.WriteLine("NULL");
        }

        private static void ActionOnNotNull(string text)
        {
            Console.WriteLine(text);
        }
    }
}