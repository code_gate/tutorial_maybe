using System;
using System.Collections.Generic;
using System.Linq;
using Functional.Maybe;
using TutorialMaybe.objects;

namespace TutorialMaybe
{
    public class MaybeEnumerableExamples
    {
        public static void MaybeExample()
        {
            var list = new List<KeyValuePair<Maybe<int>, Maybe<string>>>();
            list.Add(new KeyValuePair<Maybe<int>, Maybe<string>>(0.ToMaybe(), "zero".ToMaybe()));
            list.Add(new KeyValuePair<Maybe<int>, Maybe<string>>(Maybe<int>.Nothing, Maybe<string>.Nothing));
            list.Add(new KeyValuePair<Maybe<int>, Maybe<string>>());
            
            
            var user = list.FirstMaybe((x) => x.Key == 0.ToMaybe());
            var user2 = list.LastMaybe();
            
            var values = list.Select((x) => x.Value);
            values.ToList().ForEach(maybe => maybe.Do(Console.WriteLine));
        }
    }
}